# Count the Vowels
# Version 1.1

Enter a string and the program counts the number of vowels in the text. For added complexity have it report a sum of each vowel found.

Project idea from: https://github.com/karan/Projects/blob/master/README.md